import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { EventModule } from './events/event.module';

@Module({
  imports: [
    EventModule,
    GraphQLModule.forRoot({
      typePaths: ['./**/*.graphql'],
      installSubscriptionHandlers: true,
    }),
  ],
})
export class AppModule {}
