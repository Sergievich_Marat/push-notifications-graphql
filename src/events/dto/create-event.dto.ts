import { NewEvent } from '../../graphql';
import { IsNotEmpty } from 'class-validator'

export class CreateEventDto extends NewEvent {
  @IsNotEmpty()
  title: string;

  @IsNotEmpty()
  url: string;
}
