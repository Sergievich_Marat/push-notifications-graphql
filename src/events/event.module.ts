import { Module } from '@nestjs/common';
import { EventResolvers } from './event.resolvers';
import { EventService } from './event.service';

@Module({
  providers: [EventService, EventResolvers],
})
export class EventModule {}
