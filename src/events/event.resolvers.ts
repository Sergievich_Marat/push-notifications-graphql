import { Args, Mutation, Query, Resolver, Subscription } from '@nestjs/graphql';
import { Event } from '../graphql';
import { EventService } from './event.service';
import { CreateEventDto } from './dto/create-event.dto';
import { PubSub } from 'graphql-subscriptions'

@Resolver('Event')
export class EventResolvers {
  
  private pubsub: PubSub

  constructor(private readonly eventService: EventService) {
    this.pubsub = new PubSub()
  }

  @Query()
  async events() {
    return this.eventService.findAll();
  }

  @Mutation('createEvent')
  async create(@Args('input') args: CreateEventDto): Promise<Event> {
    const event: Event = await this.eventService.create(args);
    this.pubsub.publish('eventAdded', { eventAdded: event })
    return event;
  }

  // @Subscription(returns => Event, /*{
  //   filter: (payload, variables) => payload.eventAdded.title === variables.title,
  // }*/)
  // eventAdded() {
  //   this.pubSub.asyncIterator('eventAdded')
  // }
  @Subscription(returns => Event, {
    filter: (payload, variables) =>
      payload.eventAdded.title === variables.title,
  })
  eventAdded(@Args('title') title: string) {
    return this.pubsub.asyncIterator('eventAdded');
  }
}
