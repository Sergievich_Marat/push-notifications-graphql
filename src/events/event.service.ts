import { Injectable } from '@nestjs/common';
import { Event } from '../graphql';
import { CreateEventDto } from './dto/create-event.dto';

@Injectable()
export class EventService {
  private readonly events: Event[] = [];

  create(eventDto: CreateEventDto): Event {
    const listSize: number  = this.events.length + 1;
    const event: Event = new Event();
    event.title = eventDto.title;
    event.url = eventDto.url;
    this.events.push(event);
    return event;
  }

  findAll(): Event[] {
    return this.events;
  }
}
