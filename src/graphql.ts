
/** ------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
/* eslint-disable */
export class NewEvent {
    title: string;
    url: string;
}

export class Event {
    title: string;
    url: string;
}

export abstract class IQuery {
    abstract events(): Event[] | Promise<Event[]>;
}

export abstract class IMutation {
    abstract createEvent(input: NewEvent): Event | Promise<Event>;
}

export abstract class ISubscription {
    abstract eventAdded(title: string): Event | Promise<Event>;
}
